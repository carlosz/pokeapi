from django.core.serializers.json import Deserializer, DjangoJSONEncoder
from django.core.serializers.json import Serializer as BaseSerializer
from django.forms.models import model_to_dict

from .models import Pokemon, Stat


class Serializer(BaseSerializer):
    def get_dump_object(self, obj):
        if isinstance(obj, Pokemon):
            data = model_to_dict(obj)
            data["stats"] = list(obj.stats.all())

            if hasattr(obj, "preevolutions"):
                data["preevolutions"] = [
                    self.as_evol(p, "preevolution") for p in obj.preevolutions
                ]

            if hasattr(obj, "evolutions"):
                data["evolutions"] = [
                    self.as_evol(p, "evolution") for p in obj.evolutions
                ]
            return data
        return super().get_dump_object(obj)

    def _init_options(self):
        self.options.setdefault("cls", PokemonEncoder)
        super()._init_options()

    def as_evol(self, pokemon, type):
        return {"id": pokemon.id, "name": pokemon.name, "evolution_type": type}


class PokemonEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Stat):
            return model_to_dict(obj)
        return super().default(obj)
