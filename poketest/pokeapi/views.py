from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse

from pokeapi.models import Pokemon, Stat

from . import api


def pokemon_search(request):
    name = request.GET.get("name")
    if not name:
        return JsonResponse(
            status=400,
            data={"status": False, "message": "[name] parameter is required"},
        )

    mons = Pokemon.objects.filter(name__icontains=name).prefetch_related("stats")
    evol_ids = [i.evolution_id for i in mons]
    evol_chain = Pokemon.objects.only("id", "name", "base_pokemon_id").filter(
        evolution_id__in=evol_ids
    )

    keyed_evols = _keyd_by_id(evol_chain)

    preevolutions = {}
    for evol in evol_chain:
        if evol.base_pokemon_id:
            preevolutions[evol.id] = keyed_evols[evol.base_pokemon_id]

    evolutions = {}
    for evol in evol_chain:
        if evol.base_pokemon_id:
            evolutions.setdefault(evol.base_pokemon_id, []).append(evol)

    for mon in mons:
        mon.preevolutions = _get_preevolutions(mon.id, preevolutions)
        mon.evolutions = _get_evolutions(mon.id, evolutions)

    serialized = serialize("json", mons)
    return HttpResponse(content=serialized, content_type="application/json")


def _keyd_by_id(pokemons):
    result = {}
    for mon in pokemons:
        result[mon.id] = mon
    return result


def _get_preevolutions(id, preevolutions):
    result = []
    preevolution = preevolutions.get(id)
    while preevolution:
        result.append(preevolution)
        if preevolution.base_pokemon_id:
            preevolution = preevolutions.get(preevolution.id)
        else:
            preevolution = None
    return result


def _get_evolutions(id, evolution_chain):
    result = []
    evolutions = evolution_chain.get(id, [])
    for evolution in evolutions:
        result.append(evolution)
        result.extend(_get_evolutions(evolution.id, evolution_chain))
    return result
