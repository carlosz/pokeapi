# Generated by Django 3.1.6 on 2021-02-10 03:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("pokeapi", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="pokemon",
            name="pkid",
        ),
        migrations.AlterUniqueTogether(
            name="stat",
            unique_together={("name", "pokemon")},
        ),
    ]
