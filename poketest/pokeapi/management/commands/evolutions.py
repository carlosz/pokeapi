from django.core.management.base import BaseCommand, CommandError

import pokeapi.api as api
from pokeapi.models import Pokemon, Stat


def save_pokemon(evolution_id, pokemon, pokedata, stdout, verbose_func=None):
    """
    Saves a pokemon and its stats (updates it if the pokemon already exists in the database)
    """
    mon, _ = Pokemon.objects.update_or_create(
        id=pokemon["pkid"],
        defaults={
            "name": pokemon["name"],
            "height": pokedata["height"],
            "weight": pokedata["weight"],
            "evolution_id": evolution_id,
            "base_pokemon_id": pokemon["base_pkid"],
        },
    )

    stdout.write(f"Saving {mon}...")
    if verbose_func:
        verbose_func(pokemon, pokedata)

    mon.save()
    # TODO: can we update_or_create the stats in bulk?
    # TODO: should we delete removed stats?
    for stat in pokedata["stats"]:
        stat, _ = Stat.objects.update_or_create(
            name=stat["stat"]["name"],
            pokemon=mon,
            defaults={"value": stat["base_stat"]},
        )
        stat.save()


class Command(BaseCommand):
    help = "Retrieves pokemon evolutions for an evolution chain id"

    def add_arguments(self, parser):
        parser.add_argument("id", type=int)
        parser.add_argument("--verbose", action="store_true")

    def handle(self, *args, **options):
        evol_id = options["id"]
        pokemons = api.get_evolution_chain(evol_id)

        for pokemon in pokemons:
            pokedata = api.get_pokemon(pokemon["pkid"])
            save_pokemon(
                evol_id,
                pokemon,
                pokedata,
                self.stdout,
                self._print_pokemon if options["verbose"] else None,
            )

    def _print_pokemon(self, pokemon, pokedata):
        self.stdout.write(f"Name: {pokemon['name']}\nBase Stats:")

        for stat in pokedata["stats"]:
            self.stdout.write(f'  - {stat["stat"]["name"]}: {stat["base_stat"]}')

        height = pokedata["height"] / 10  # from decimeters to meters
        weight = pokedata["weight"] / 10  # from hectograms to kilograms
        self.stdout.write(
            f"Height: {height:.2f}m\n"
            f"Weight: {weight:.2f}kg\n"
            f"Id: #{pokemon['pkid']:03}\n\n"
        )
