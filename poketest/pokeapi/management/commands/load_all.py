from django.core.management.base import BaseCommand, CommandError

import pokeapi.api as api
from pokeapi.models import Pokemon, Stat

from .evolutions import save_pokemon


class Command(BaseCommand):
    help = "Retrieves pokemon evolutions and saves them in bulk"

    def add_arguments(self, parser):
        parser.add_argument("--start", type=int, default=0, help="starting offset")
        parser.add_argument("--end", type=int, default=None, help="end")

    def handle(self, *args, **options):
        for evol_id in api.get_all_evolution_chains(
            start=options["start"], end=options["end"]
        ):
            self.stdout.write(f"Loading evolution chain id [{evol_id}]...")
            pokemons = api.get_evolution_chain(evol_id)
            for pokemon in pokemons:
                pokedata = api.get_pokemon(pokemon["pkid"])
                save_pokemon(evol_id, pokemon, pokedata, self.stdout)
