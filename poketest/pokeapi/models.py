from django.db import models


class Pokemon(models.Model):
    def stats_default():
        return {}

    name = models.CharField(max_length=200, db_index=True)
    height = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    evolution_id = models.IntegerField(default=0, db_index=True)
    base_pokemon = models.ForeignKey("self", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f"#{self.id:03} {self.name}"


class Stat(models.Model):
    name = models.CharField(max_length=200)
    value = models.IntegerField(default=0)
    pokemon = models.ForeignKey(Pokemon, related_name="stats", on_delete=models.CASCADE)

    class Meta:
        unique_together = (("name", "pokemon"),)

    def __str__(self):
        return f"{self.name} = {self.value}"
