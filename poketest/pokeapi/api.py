from itertools import chain

import requests

POKEAPI = "https://pokeapi.co/api/v2/"
EVOLUTION_CHAIN = POKEAPI + "evolution-chain/"
SINGLE_POKEMON = POKEAPI + "pokemon/"


def _unwrap_chain(chain, base_pkid=None):
    "Turns nested evolutions into a straight list"
    url = chain["species"]["url"]
    pkid = int(url.split("/")[-2])
    yield dict(chain["species"], pkid=pkid, base_pkid=base_pkid)
    for i in chain["evolves_to"]:
        yield from _unwrap_chain(i, pkid)


def get_evolution_chain(id: int):
    """
    Returns all evolution chains
    Note: id is not the Pokemon number, it goes from 1 (pk #1) to 475 (pk #898)
    """
    res = requests.get(f"{EVOLUTION_CHAIN}{id}/")
    res.raise_for_status()
    data = res.json()
    return _unwrap_chain(data["chain"])


def get_all_evolution_chains(start=0, end=None):
    "Returns a list of all available evolution chain ids"
    offset = start
    limit = 100
    while True:
        res = requests.get(f"{EVOLUTION_CHAIN}?offset={offset}&limit={limit}")
        res.raise_for_status()
        data = res.json()
        yield from (int(i["url"].split("/")[-2]) for i in data["results"])
        offset += limit
        if (end is not None and offset >= end) or not data["next"]:
            break


def get_pokemon(id: int):
    "Returns the data for a single pokemon"
    res = requests.get(f"{SINGLE_POKEMON}{id}/")
    res.raise_for_status()
    data = res.json()
    return data
