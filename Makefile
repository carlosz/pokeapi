lint:
	@poetry run isort poketest
	@poetry run black poketest

requirements.txt: poetry.lock pyproject.toml
	poetry export -f requirements.txt > requirements.txt

dev:
	poetry run python poketest/manage.py runserver
