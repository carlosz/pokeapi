# PokeAPI
Test para el puesto de desarrollador Back-end

## Instalar dependencias

Usando [Poetry](https://python-poetry.org/):

- `poetry install`

Usando pip:

- `python -m pip install -r requirements.txt`

## Correr servidor

- `poetry shell` (solo si se esta usando Poetry)
- `cd poketest`
- `python manage.py runserver`

## Levantar base de datos

Usando sqlite por default:

- `python manage.py migrate`

Usando MySql/Mariadb:

- Instalar las dependencias opcionales para mysql:
  - Con Poetry: `poetry install -E mysql`
  - Con pip: `python -m pip install -r optional-requirements.txt`
- Configurar campo `DATABASES` en archivo `poketest/poketest/settings.py`
    - Nota: Viene con un docker-compose.yml listo para levantar una base de datos
      solo es necesario correr `docker-compose up` y descomentar la configuración en
      `settings.py`
- `python manage.py migrate`

## Comandos

- Para cargar una cadena de evolución: `python poketest/manage.py evolutions <id> [--verbose]`
    - Nota: `id` no es el `id` de un pokemon sino el `id` de una cadena de evolución, por 
      ejemplo **eevee** tiene `id` `133` pero su cadena de evolución tiene `id` `67`
- Para cargar todo de una sola vez utilizar: `python poketest/manage.py load_all`

## Web Services

- El endpoint para buscar pokemones es: `http://127.0.0.1:8000/pokemon/?name=<name>`
